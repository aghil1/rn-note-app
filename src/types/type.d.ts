type Note = {
   id?: string | number[];
   title: string;
   note: string;
   date_add?: string;
   date_update?: string;
}