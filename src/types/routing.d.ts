type RootStackParam = {
   Auth: undefined;
   AddEdit: {
      data?: Note;
   };
   ListScreen: undefined;
   Splash: undefined;
   Welcome: undefined;
};