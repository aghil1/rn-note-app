import React from 'react';
import {StyleSheet, Text} from 'react-native';

interface props {
   text: string,
   numberOfLines?: number,
   size?: number | 14,
   color?: string,
   lineHeight?: number,
   style?: any,
   testID?: string | '',
   onTextLayout?: any,
   fontFamily?: string
}

export const TextRegular = ({
   text,
   numberOfLines,
   onTextLayout,
   size = 14,
   color,
   lineHeight = size * 1.5,
   style,
   testID = '',
}: props) => {
   return (
      <Text
         ellipsizeMode="tail"
         style={[
            styles.regularText,
            {fontSize: size, color: color, lineHeight: lineHeight},
            style,
         ]}
         onTextLayout={onTextLayout}
         numberOfLines={numberOfLines}
         accessibilityLabel={testID}
         testID={testID}>
         {text}
      </Text>
   );
};

export const TextMedium = ({
   text,
   numberOfLines,
   size = 14,
   color,
   lineHeight = size * 1.5,
   style,
   testID = '',
}: props) => {
   return (
      <Text
         ellipsizeMode="tail"
         style={[
            styles.mediumText,
            {fontSize: size, color: color, lineHeight: lineHeight},
            style,
         ]}
         numberOfLines={numberOfLines}
         accessibilityLabel={testID}
         testID={testID}>
         {text}
      </Text>
   );
};

export const TextBold = ({
   text,
   numberOfLines,
   size = 14,
   color,
   lineHeight = size * 1.5,
   style,
   testID = '',
}: props) => {
   return (
      <Text
         style={[
            styles.boldText,
            {fontSize: size, color: color, lineHeight: lineHeight},
            style,
         ]}
         ellipsizeMode="tail"
         numberOfLines={numberOfLines}
         accessibilityLabel={testID}
         testID={testID}>
         {text}
      </Text>
   );
};

const styles = StyleSheet.create({
   regularText: {
      fontFamily: 'HelveticaNeue-Regular'
   },
   mediumText: {
      fontFamily: 'HelveticaNeue-Medium'
   },
   boldText: {
      fontFamily: 'HelveticaNeue-Bold'
   }
});