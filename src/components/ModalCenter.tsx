import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {
   Modal,
   View,
} from 'react-native';

const ModalCenter = forwardRef((props: {
   children: any
}, ref) => {
   const [isVisible, setIsVisible] = useState(false);
   useImperativeHandle(ref, () => {
      return {
         show: () => setIsVisible(true),
         hide: () => setIsVisible(false),
      };
   });
   return (
      <Modal
         transparent
         visible={isVisible}
         animationType='slide'
      >
         <View style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.4)',
            justifyContent: 'center',
            alignItems: 'center'
         }}>
            <View style={{
               width: '90%',
               backgroundColor: '#fff',
               borderRadius: 6
            }}>
               {props.children}
            </View>
         </View>
      </Modal>
   )
})

export default ModalCenter;