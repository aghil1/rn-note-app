import React, {memo} from 'react';
import {
   View,
   StyleSheet,
   TextInput,
   TouchableOpacity
} from 'react-native';
import {TextBold, TextMedium, TextRegular} from './Text';
import {Touchable} from './Touchable';

interface props {
   title: string,
   note: string,
   index: number,
   onAction: () => void,
   date_add: string,
   date_update: string
}

const CardItem = ({
   title, 
   index, 
   note, 
   onAction,
   date_add,
   date_update
} : props) => {
   return(
      <Touchable
         key={`itemNote-${index.toString()}`}
         onPress={onAction}
         style={{
            paddingVertical: 15,
            width: '100%'
         }}
      >
         <TextBold
            text={title}
            color='#000'
            size={16}
         />
         <TextRegular
            text={note}            
         />
         <View style={styles.row}>
            <TextMedium
               text="Date Add : "
               color='#000'
            />
            <TextRegular
               text={date_add}
            />
         </View>
         <View style={styles.row}>
            <TextMedium
               text="Date Update : "
               color='#000'
            />
            <TextRegular
               text={date_update}
            />
         </View>
      </Touchable>
   )
}

const styles = StyleSheet.create({
   row: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 10
   }
})

const areEqual=(prevProps: any,nextProps: any)=>{
   return prevProps.note===nextProps.note
}

export default memo(CardItem, areEqual);