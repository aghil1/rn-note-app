import React from 'react';
import {TouchableOpacity} from 'react-native';

interface props {
   children: any,
	onPress: () => void,
	disabled?: boolean,
	style?: object,
	testID?: string,
}

export const Touchable = ({
	children,
	onPress,
	disabled,
	style,
	testID,
}: props) => {
	return (
		<TouchableOpacity
			accessible={true}
			accessibilityLabel={testID}
			testID={testID}
			style={style}
			activeOpacity={0.8}
			onPress={onPress}
			disabled={disabled}>
			{children}
		</TouchableOpacity>
	);
};