import { TextBold } from './Text';
import { TextMedium } from './Text';
import { TextRegular } from './Text';
import { FieldButton, OutlineButton } from './Button';
import ModalCenter from './ModalCenter';

export { TextBold, TextMedium, TextRegular, FieldButton, OutlineButton, ModalCenter }