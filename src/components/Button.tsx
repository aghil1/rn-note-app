import React from 'react';
import { Image, StyleSheet, Platform } from 'react-native';
import {
   Touchable,
} from './Touchable';
import {
   TextBold,
   TextRegular,
   TextMedium
} from './Text'
import {Colors} from '../styles';

interface props {
   textColor?: string,
   textButton: string,
   backgroundColor?: string,
   style?: object,
   onPress: () => void,
   testID?: string,
   disabled?: boolean,
   icon?: any,
   iconSize?: number,
   iconStyle?: any,
   textSize?: number,
   textStyle?: object,
   typeText?: string,
   borderColor?: string
}

export function FieldButton({
   textColor = Colors.WHITE,
   textButton = 'Button',
   backgroundColor = Colors.AQUA,
   style,
   onPress,
   testID,
   disabled = false,
   icon,
   iconStyle,
   iconSize = 20,
   textSize = 14,
   textStyle,
   typeText = 'regular'
}: props) {
   return (
      <Touchable
         testID={testID}
         onPress={onPress}
         disabled={disabled}
         style={[
            styles.container,
            { backgroundColor },
            styles.rowStyle,
            Platform.OS === 'ios' && styles.bottomWithPadding,
            style,
         ]}
      >
         {
            !icon ? null
         : (
            <Image
               source={icon}
               resizeMode="contain"
               style={[styles.iconStyle, iconStyle, { width: iconSize, height: iconSize }]}
            />
         )}
            {
               typeText === 'bold' ?
                  <TextBold size={textSize} style={textStyle} color={textColor} text={textButton} />
               :
               typeText === 'medium' ?
                  <TextMedium size={textSize} style={textStyle} color={textColor} text={textButton} />
               :
                  <TextRegular size={textSize} style={textStyle} color={textColor} text={textButton} />
            }
      </Touchable>
   );
}

export function OutlineButton({
   textColor = Colors.BLACK,
   textButton = 'Button',
   backgroundColor = Colors.WHITE,
   borderColor = Colors.BLACK,
   style,
   onPress,
   testID,
   icon,
   textStyle,
   iconSize = 20,
   disabled = false,
   typeText = 'regular',
   textSize = 14
}: props) {
   return (
      <Touchable
         onPress={onPress}
         testID={testID}
         disabled={disabled}
         style={[
            styles.container,
            styles.outline,
            { backgroundColor, borderColor },
            styles.rowStyle,
            Platform.OS === 'ios' && styles.bottomWithPadding,
            style,
         ]}>
         {
            !icon ? null
         : (
            <Image
               source={icon}
               resizeMode="contain"
               style={[styles.iconStyle, { width: iconSize, height: iconSize }]}
            />
         )}
         {
            typeText === 'bold' ?
               <TextBold size={textSize} style={textStyle} color={textColor} text={textButton} />
            :
            typeText === 'medium' ?
               <TextMedium size={textSize} style={textStyle} color={textColor} text={textButton} />
            :
               <TextRegular size={textSize} style={textStyle} color={textColor} text={textButton} />
         }
      </Touchable>
   );
}

const styles = StyleSheet.create({
   container: {
      backgroundColor: Colors.AQUA,
      padding: 10,
      width: '100%',
      alignSelf: 'flex-start',
      borderRadius: 6,
      alignItems: 'center',
      justifyContent: 'center',
   },
   outline: {
      borderWidth: 1,
      borderColor: Colors.AQUA,
   },
   rowStyle: {
      flexDirection: 'row',
   },
   iconStyle: {
      marginRight: 4,
   },
   bottomWithPadding: {
      paddingBottom: 7,
   },
});