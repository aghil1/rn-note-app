import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";

const StoreManager = {
   addData: async (payload: Note) => {
      try {
         let getData = await AsyncStorage.getItem('dataNote');
         let data = getData ? JSON.parse(getData) : [];

         payload.id = `${moment(new Date()).millisecond()}`;
         payload.date_add = moment(new Date()).format('DD MM YYYY, HH:mm:ss');
         payload.date_update = moment(new Date()).format('DD MM YYYY, HH:mm:ss');
         await AsyncStorage.setItem('dataNote', JSON.stringify([payload, ...data]));
         return true;
      } catch (error) {
         throw new Error("Error while adding data");
      }
   },
   editData: async (id: string, payload: Note) => {
      try {
         let getData = await AsyncStorage.getItem('dataNote');
         let data = getData ? JSON.parse(getData) : [];

         payload.date_update = moment(new Date()).format('DD MM YYYY, HH:mm:ss');
         data = data.map((value: Note) => {
            if (value.id === id) {
               return {
                  ...value,
                  ...payload
               }
            }
            return value;
         })
         await AsyncStorage.setItem('dataNote', JSON.stringify([...data]));
         return true;
      } catch (error) {

      }
   },
   listData: async () => {
      let getData = await AsyncStorage.getItem('dataNote');
      let data = getData ? JSON.parse(getData) : [];
      return data;
   },
   storePassword: async (payload: string) => {
      try {
         AsyncStorage.setItem('password', payload);
      } catch (error) {
         throw new Error('Error while generate password');
      }
   },
   checkPassword: async (payload: string) => {
      try {
         const password = await AsyncStorage.getItem('password');
         if (password === payload) {
            return true;
         } else {
            return false;
         }
      } catch (error) {
         throw new Error('Error while check password');
      }
   },

}

export default StoreManager;