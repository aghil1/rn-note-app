import React, {useEffect, useState, useCallback} from 'react';
import {
   View,
   TextInput,
   Platform,
   Alert,
   StyleSheet
} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {TextBold, TextRegular, FieldButton, TextMedium} from '../../components';
import ReactNativeBiometrics from 'react-native-biometrics';
import { Colors } from '../../styles';
import StoreManager from '../../utils/StoreManager';

const rnBiometrics = new ReactNativeBiometrics();

interface props {
   navigation: StackNavigationProp<RootStackParam, 'Auth'>;
   route: RouteProp<RootStackParam, 'Auth'>
}

const Login: React.FC<props> = ({navigation}) => {

   const [password, setPassword] = useState('');
   const [disable, setDisable] = useState(false);

   function checkBiometrics() {
      rnBiometrics.isSensorAvailable()
      .then((resultObject) => {
         const { available } = resultObject

         if(available) {
            verifyBiometrics();
         } else {
            setDisable(true)
         }
      })
   }

   function verifyBiometrics() {
      let prompMsg = ''
      if(Platform.OS === 'android') {
         prompMsg = 'Confirm fingerprint'
      } else {
         prompMsg = 'Confirm FaceId'
      }
      rnBiometrics.simplePrompt({promptMessage: prompMsg})
         .then((resultObject) => {
            const { success } = resultObject

            if (success) {
               setDisable(false);
               console.log('successful biometrics provided')
            } else {
               setDisable(true)
            }
         })
         .catch(() => {
            console.log('biometrics failed')
         })
   }

   async function checkPassword() {
      let isMatch = await StoreManager.checkPassword(password);
      if(isMatch) {
         navigation.replace('ListScreen');
      } else {
         Alert.alert('Ooopss..', "Password doesn't match.")
      }
   }

   const setMessage = useCallback(() => {
      let msg = ''
      if(disable) {
         if(Platform.OS === 'android') {
            msg = 'Fingerprint is not detected, you cannot continue the process.'
         } else {
            msg = 'FaceId is not detected, you cannot continue the process.'
         }
      } else {
         msg = "Enter your previously saved password" 
      }
      return msg;
   }, [disable]);

   useEffect(() => {
      checkBiometrics();
   },[])

   return(
      <View style={{flex: 1, paddingHorizontal: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'}}>
         <TextMedium
            text={setMessage()}
            size={16}
            color='#000'
            style={{textAlign: 'center'}}
         />
         <TextInput
            testID='setupPassword'
            placeholder='Enter your password'
            value={password}
            onChangeText={(text) => setPassword(text)}
            style={styles.txtInput}
            secureTextEntry
         />
         <FieldButton
            textButton='Save Password'
            onPress={checkPassword}
            backgroundColor={disable ? '#dedede' : Colors.ORANGE}
            style={{
               marginTop: 20,
               width: '100%'
            }}
            disabled={disable}
         />
      </View>
   )
}

const styles = StyleSheet.create({
   txtInput: {
      width: '100%',
      marginVertical: 15,
      borderRadius: 6,
      borderColor: Colors.SOFT_BLUE,
      borderWidth: 1,
      paddingHorizontal: 10
   },
})

export default Login;