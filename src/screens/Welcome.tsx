import React, {useEffect, useRef, useState} from 'react'
import {
   View,
   Platform,
   TextInput,
   Image,
   StyleSheet
} from 'react-native'
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TextBold, TextMedium, TextRegular, FieldButton, ModalCenter} from '../components';
import ReactNativeBiometrics from 'react-native-biometrics';
import { Colors } from '../styles';
import StoreManager from '../utils/StoreManager';

const rnBiometrics = new ReactNativeBiometrics();

interface props {
   navigation: StackNavigationProp<RootStackParam, 'Welcome'>;
   route: RouteProp<RootStackParam, 'Welcome'>
}

const Welcome: React.FC<props> = ({navigation}) => {

   const modalCenterRef = useRef(null);
   const [password, setPassword] = useState('');
   const [disable, setDisable] = useState(false);   

   function checkBiometrics() {
      rnBiometrics.isSensorAvailable()
      .then((resultObject) => {
         const { available } = resultObject

         if(available) {
            verifyBiometrics();
         } else {
            setDisable(true);
         }
      })
   }

   function verifyBiometrics() {
      let prompMsg = ''
      if(Platform.OS === 'android') {
         prompMsg = 'Confirm fingerprint'
      } else {
         prompMsg = 'Confirm FaceId'
      }
      rnBiometrics.simplePrompt({promptMessage: prompMsg})
         .then((resultObject) => {
            const { success } = resultObject

            if (success) {
               console.log('successful biometrics provided')
               modalCenterRef?.current.show();
            } else {
               console.log('user cancelled biometric prompt')
            }
         })
         .catch(() => {
            console.log('biometrics failed')
         })
   }

   async function generatePassword() {
      await StoreManager.storePassword(password);
      await AsyncStorage.setItem('isLoggedIn', '1');
      navigation.replace('Auth')
   }

   function setMessage() {
      let msg = ''
      if(disable) {
         if(Platform.OS === 'android') {
            msg = 'Fingerprint is not detected, you cannot continue the process.'
         } else {
            msg = 'FaceId is not detected, you cannot continue the process.'
         }
      } else {
         msg = "Let's setup password to protect your note's"
      }
      return msg;
   }

   return(
      <View style={styles.container}>
         <TextMedium
            text='Welcom to Note App'
            size={18}
            color='#000'
         />
         <TextRegular
            text={setMessage()}
         />
         <FieldButton
            textButton='Setup Password'
            onPress={checkBiometrics}
            backgroundColor={Colors.ORANGE}
            style={{
               marginTop: 20,
               width: '90%',
               alignSelf: 'center'
            }}
            disabled={disable}
         />
         {SetUpPassword()}
      </View>
   )

   function SetUpPassword() {
      return(
         <ModalCenter
				ref={modalCenterRef}
			>
				<View style={{padding: 15}}>
					<TextBold
						text="Setup Password"
                  size={18}
					/>
               <TextRegular
                  text="Setup Your Password to secure your note"
               />
               <TextInput
                  testID='setupPassword'
                  placeholder='Enter your password'
                  value={password}
                  onChangeText={(text) => setPassword(text)}
                  style={styles.txtInput}
                  secureTextEntry
               />
               <FieldButton
                  textButton='Save Password'
                  onPress={() => {
                     modalCenterRef?.current?.hide();
                     generatePassword();
                  }}
                  backgroundColor={Colors.ORANGE}
                  style={{
                     marginTop: 20,
                     width: '90%',
                     alignSelf: 'center'
                  }}
               />
				</View>
			</ModalCenter>
      )
   }

}

const styles = StyleSheet.create({
   txtInput: {
      marginVertical: 15,
      borderRadius: 6,
      borderColor: Colors.SOFT_BLUE,
      borderWidth: 1,
      paddingHorizontal: 10
   },
   container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center'
   }
})

export default Welcome;