import React, { useEffect, useState } from 'react'
import {
   View,
   Text,
   StyleSheet,
   FlatList
} from 'react-native'
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import StoreManager from '../../utils/StoreManager';
import {useIsFocused} from '@react-navigation/native';
import {FieldButton, TextBold, TextMedium, TextRegular} from '../../components';
import { Colors } from '../../styles';
import CardItem from '../../components/CardItem';

interface props {
   navigation: StackNavigationProp<RootStackParam, 'ListScreen'>;
   route: RouteProp<RootStackParam, 'ListScreen'>
}

const ListScreen: React.FC<props> = ({navigation}) => {
   const isFocused = useIsFocused();
   const [listData, setListData] = useState<Note[]>([]);

   async function getList() {
      console.log('reget data');      
      const data = await StoreManager.listData();
      setListData(data);
   }   

   const renderItem = ({item, index}: {item: Note; index: number}) => {
      return (
         <CardItem
            index={index}
            title={item.title}
            note={item.note}
            date_add={item.date_add || '-'}
            date_update={item.date_update || '-'}
            onAction={() => navigation.navigate('AddEdit', {data: item})}
         />
      )
   }

   useEffect(() => {
      getList();
   },[isFocused])

   return(
      <View style={{
         flex: 1,
         backgroundColor: '#fff'
      }}>
         <FlatList
            contentContainerStyle={{flex: 1, paddingHorizontal: 15}}
            data={listData}
            keyExtractor={(item, index) => index.toString()}
            renderItem={renderItem}
            ItemSeparatorComponent={() => (
               <View style={styles.lineSeparator} />
            )}
            ListEmptyComponent={() => (
               <View style={styles.viewEmpty}>
                  <TextMedium
                     text="Note data does not exist, let's add note data. your notes will be stored safely"
                     color='#000'
                     style={{textAlign: 'center'}}
                  />
               </View>
            )}
         />
         <FieldButton
            testID='fb-action'
            icon={require('../../assets/icons/plus_ic.png')}
            iconStyle={{tintColor: '#fff', marginLeft: 4}}
            textButton=''
            onPress={() => navigation.navigate("AddEdit", {})}
            style={styles.floatingButton}
            backgroundColor={Colors.PRIMARY}
         />
      </View>
   )
}

const styles = StyleSheet.create({
   lineSeparator: {
      width: '100%', 
      height: 1, 
      backgroundColor: Colors.BLACK
   },
   viewEmpty: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
   },
   floatingButton: {
      width: 50,
      height: 50,
      borderRadius: 25,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      bottom: 10,
      right: 10
   }
})

export default ListScreen;