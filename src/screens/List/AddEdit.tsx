import React, {useEffect, useState} from 'react';
import {
   View,
   TextInput,
   Alert,
   StyleSheet
} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Colors} from '../../styles';
import {TextRegular, FieldButton} from '../../components';
import StoreManager from '../../utils/StoreManager';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CryptoJS from 'crypto-js';

interface props {
   navigation: StackNavigationProp<RootStackParam, 'AddEdit'>;
   route: RouteProp<RootStackParam, 'AddEdit'>
}

const AddEdit: React.FC<props> = ({navigation, route}) => {
   const routeParams = route.params;
   const [data, setData] = useState<Note>({
      title: '',
      note: '',
   });


   console.log('route edit', route);

   async function addData() {
      const password = (await AsyncStorage.getItem('password')) || '';
      const payload = {
         ...data,
         note: CryptoJS.AES.encrypt(data.note, password).toString()
      };
      console.log('payload add', payload);
      const add = await StoreManager.addData(payload);
      console.log('add data', add);
      if(add) {
         setData({
            title: '',
            note: ''
         })
         Alert.alert('Success', 'Successfully adding your note', [
            {
               text: 'Oke',
               onPress: () => navigation.goBack()
            }
         ])
      }
   }

   async function editData() {
      const password = (await AsyncStorage.getItem('password')) || '';
      const payload = {
         ...data,         
         note: CryptoJS.AES.encrypt(data.note, password).toString()
      };      
      console.log('payload edit', payload);
      console.log('cek note', data);

      const edit = await StoreManager.editData(routeParams?.data.id, payload);
      console.log('edit data', edit);     
      if(edit) {
         setData({
            title: '',
            note: ''
         })
         Alert.alert('Success', 'Successfully edit your note', [
            {
               text: 'Oke',
               onPress: () => navigation.goBack()
            }
         ])
      }
   }

   async function getData() {
      if (routeParams?.data) {
         const password = (await AsyncStorage.getItem('password')) || '';
         setData({
            title: routeParams.data?.title,
            note: CryptoJS.AES.decrypt(routeParams.data?.note, password).toString(CryptoJS.enc.Utf8),
         });
      }
   
   }
   
   useEffect(() => {
      if(routeParams) {
         getData();
      }
   },[])
   
   return(
      <View style={styles.container}>
         <TextInput
            placeholder='Enter title of your note'
            value={data.title}
            editable={routeParams?.data ? false : true}
            onChangeText={(text) => setData({...data, title: text})}
            style={styles.txtInput}
         />
         <TextInput
            placeholder='Enter your note'
            value={data.note}
            onChangeText={(text) => setData({...data, note: text})}
            style={[styles.txtInput, {marginTop: 20}]}
         />
         <FieldButton
            testID='btn-sv'
            textButton='Submit'
            style={{width: 200, marginTop: 20, alignSelf: 'center',}}
            backgroundColor={Colors.ORANGE}
            onPress={() => {
               if(routeParams?.data) {
                  editData()
               } else {
                  addData()
               }
            }}
            disabled={data.title === '' || data.note === ''}
         />
      </View>
   )
}

const styles = StyleSheet.create({
   txtInput: {
      width: '90%',
      borderColor: Colors.AQUA,
      borderRadius: 4,
      borderWidth: 1,
      paddingHorizontal: 10
   },
   container: {
      flex: 1, 
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center'
   }
})

export default AddEdit;
