import React, {useEffect, useRef} from 'react'
import {
   View,
   Text,
   Image,
   StyleSheet
} from 'react-native'
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {TextBold, TextMedium, TextRegular} from '../components';


interface props {
   navigation: StackNavigationProp<RootStackParam, 'Splash'>;
   route: RouteProp<RootStackParam, 'Splash'>
}

const SplashScreen: React.FC<props> = ({navigation}) => {

   async function checkHasLogin () {
      let isLoggedIn = await AsyncStorage.getItem('isLoggedIn');
      if(isLoggedIn !== undefined)  {
         if(isLoggedIn === '1') {
            navigation.replace('Auth');
         } else {
            await AsyncStorage.setItem('isLoggedIn', '0')
            navigation.replace('Welcome');
         }
      }      
   }

   useEffect(() => {
      setTimeout(() => {
         checkHasLogin();
      }, 1500);
   },[])

   return(
      <View style={styles.container}>
         <TextMedium
            text='Add Note App'
            size={18}
            color='#000'
         />
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center'
   }
})

export default SplashScreen;