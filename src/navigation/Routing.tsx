import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
//screens
import SplashScreen from '../screens/SplashScreen';
import Login from '../screens/Auth/Login';
import Welcome from '../screens/Welcome';
import ListScreen from '../screens/List/ListScreen';
import AddEdit from '../screens/List/AddEdit';

const Stack = createStackNavigator<RootStackParam>();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen
          name="Splash"
          component={SplashScreen}
        />
        <Stack.Screen
          name="Welcome"
          component={Welcome}
        />
        <Stack.Screen
          name="Auth"
          component={Login}
        />
        <Stack.Screen
          name="ListScreen"
          component={ListScreen}
        />
        <Stack.Screen
          name="AddEdit"
          component={AddEdit}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
  
}

export default Routing;